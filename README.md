 

## Overview

````  
Features include:

 * Molecular modeller with automatic force-field based geometry optimization
 * Molecular Mechanics including constraints and conformer searches
 * Visualization of molecular orbitals and general isosurfaces
 * Visualization of vibrations and plotting of vibrational spectra
 * Support for crystallographic unit cells
 * Input generation for the Gaussian, GAMESS and MOLPRO quantum chemistry
   packages
 * Flexible plugin architecture and Python scripting
File formats Avogadro can read include PDB, XYZ, CML, CIF, Molden, as well as Gaussian, GAMESS and MOLPRO output.
````  



## References

[1.] https://www.ebi.ac.uk/chebi/searchId.do?chebiId=53550

[2.] https://chemapps.stolaf.edu/jmol/docs/examples-11/new.htm
